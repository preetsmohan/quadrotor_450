#include "graph.h"
#include "tests.h"
#include "a_star.h"
#include <iostream>
#include <fstream>
#include <time.h>



using namespace std;


int main() {
	clock_t t1, t2;
	t1 = clock();
	Astar();
	t2 = clock();

	double diff ((double)t2 - (double)t1);

	double seconds = diff / CLOCKS_PER_SEC;
	cout << "Time of run: " << seconds << endl;

	return 0;
}