// File for test cases

#include "graph.h"
#include <assert.h>

using namespace std;


void testPosition() {

	// Test creating a position with no input:
	Position p;
	assert(p.x == 0);
	assert(p.y == 0);
	assert(p.z == 0);

	// Test creating a position with input:
	Position q(0, 3, 4);
	assert(q.x == 0);
	assert(q.y == 3);
	assert(q.z == 4);

	// Test assingment operator:
	p = q;
	assert(p.x == 0);
	assert(p.y == 3);
	assert(p.z == 4);

	// Test distance calculator:
	Position r;
	double distance = r.dist(q);
	assert(distance == 5);







}


