// Written by: Rehan Nawaz

#include "graph.h"

using namespace std;

//// Class helper functions and general functions:

// POSTION MEMBER FUNCTIONS:


// Calculating Euclidean 3D distance:
double Position::dist(Position f) {
	
	return pow(pow(x - f.x, 2.0) + pow(y - f.y, 2.0) + pow(z - f.z, 2.0), 0.5);
}

// Sets position as visited:
void Position::visited() {
	visit = true;
}

// Sets position as visited:
void Position::unVisited() {
	visit = false;
}

// Assignment operator:
Position& Position::operator= (const Position& rhs) {
	// do not do self assignment:
	if (this == &rhs) {
		return *this;
	}

	else {
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		visit = rhs.visit;
	}

	return *this;
}

bool Position::operator==(const Position& rhs) {
	if (rhs.x == x && rhs.y == y && rhs.z == z && rhs.visit == visit) {
		return true;
	}
	else {
		return false;
	}
}


// GRAPH MEMBER FUNCTIONS:


// Finds all unvisited nodes of graph and returns them as subgraph:
Graph Graph::findUnvisited() {
	Graph unvisited;

	for (int i = 0; i < pos.size(); i++) {
		if (!pos[i].visit) {
			unvisited.pos.push_back(pos[i]);
		}
	}

	return unvisited;
}

// Return all unvisited graph nodes:
int Graph::findUnvisitedSize() {
	int count = 0;
	for (int i = 0; i < pos.size(); ++i) {
		if (!pos[i].visit) {
			count++;
		}
	}

	return count;
}

// Find minimum distance edge from unvisited nodes:
//  If only single node remaining, return 0.
double Graph::findMinDist() {
	double minDist = 0;
	Graph unvisited = findUnvisited();

	for (int i = 0; i < unvisited.pos.size(); i++) {
		for (int j = i + 1; j < unvisited.pos.size(); j++) {
			if (minDist == 0 || unvisited.pos[i].dist(unvisited.pos[j]) < minDist) {
				minDist = unvisited.pos[i].dist(unvisited.pos[j]);
			}
		}
	}

	return minDist;
}

// Set all positions as unvisited in graph:
void Graph::resetGraph() {
	for (int i = 0; i < pos.size(); ++i) {
		pos[i].visit = false;
	}
}

// Assignment operator:
Graph& Graph::operator= (const Graph& rhs) {
	// do not do self assignment:
	if (this == &rhs) {
		return *this;
	}

	else {
		pos = rhs.pos;
	}
}

// Calculates heuristic for remaining unvisited nodes:
double Graph::heuristic() {
	double minDist = findMinDist();
	int unvisited_size = findUnvisitedSize();

	 return minDist*(unvisited_size - 1); 
	// return 0;  for Dijsktra
}

// Equality operator:
// Returns true if same nodes in both graphs:
bool Graph::operator==(const Graph& rhs) {
	if (rhs.pos.size() != pos.size()) {
		return false;
	}

	for (int i = 0; i < pos.size(); i++) {
		if (!(pos[i] == rhs.pos[i])) {
			return false;
		}
	}

	return true;

}


// GENERAL FUNCTIONS:


// Create Graph from input file:
Graph file2graph() {
	ifstream in ("test01.txt");
	double x, y, z = 0, throwaway;
	Graph g;

	if (!in.is_open()) {
		cerr << "File could not be opened\n";
		exit(1);
	}

	while(!in.eof()) {
		in >> throwaway >> x >> y;
		Position p(x, y, z);
		g.pos.push_back(p);
	}

	return  g;
}