#include "a_star.h"

using namespace std;

// Generates the next level of starnode, and puts them into priority queue:
void genLevel(Starnode* sn, priority_queue<Starnode*, vector<Starnode*>, Greater>& pq) {
	
	// Case where you haven't reached the start position yet:
	if (sn->unvisited.pos.size() > 0) {
		for (int i = 0; i < sn->unvisited.pos.size(); i++) {
			if (!sn->unvisited.pos[i].visit) {

				// Create next position for Starnode and graph of unvisited nodes:
				Position next_pos = sn->unvisited.pos[i];
				sn->unvisited.pos[i].visited();
				Starnode* next_star = new Starnode(sn->unvisited.pos[i], sn->unvisited, sn);

				// Calulate the cost to current node from beginning:
				next_star->current_cost = sn->current_cost + next_star->current.dist(sn->current);
				sn->unvisited.pos[i].unVisited();

				pq.push(next_star);
			}
		}
	}

	// Case where we need to add start position to the priority queue:
	// NOTE: need to change this everytime the start position changes:
	else {
		Starnode* next_star = new Starnode(Position(0, 0, 0), Graph(), sn);
		next_star->current_cost = sn->current_cost + next_star->current.dist(sn->current);
		//next_star->current.visited();
		pq.push(next_star);
	}

	return;
}


