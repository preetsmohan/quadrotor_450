// Comparator class for priority queue for Starnode (min or max???)
class Compare {
public:	
	bool operator() (const Starnode& a, const Starnode& b) const {
		return a.heuristic_cost < b.heuristic_cost;
	}
};