// File for test cases

#include "graph.h"
#include "a_star.h"
#include <algorithm>
#include <stack>
#include <assert.h>

using namespace std;



// Basic position testing:
void testPosition() {

	// Test creating a position with no input:
	Position p;
	assert(p.x == 0);
	assert(p.y == 0);
	assert(p.z == 0);

	// Test creating a position with input:
	Position q(0, 3, 4);
	assert(q.x == 0);
	assert(q.y == 3);
	assert(q.z == 4);

	// Test copy constructor:
	Position r = q;
	assert(r.x == 0);
	assert(r.y == 3);
	assert(r.z == 4);

	// Test assignment operator:
	p = q;
	assert(p.x == 0);
	assert(p.y == 3);
	assert(p.z == 4);

	// Test distance calculator:
	Position s;
	double distance = s.dist(q);
	assert(distance == 5);

	// Test visited:
	s.visited();
	assert(s.visit == true);

}

// basic graph testing:
void testGraph() {

	// create graph from file:
	Graph g = file2graph();

	// General Constructor:
	Graph h;
	assert(h.pos.size() == 0);

	// Copy Constructor:
	Graph i = g;
	assert(i.pos.size() == 5);

	// Asignment operator:
	h = i;
	assert(h.pos.size() == 5);

	// Test minimum distance finder:
	//  Note: tested in gdb:
	double dist = h.findMinDist();
	assert(dist < 1);
	h.pos[3].visit = true;
	dist = h.findMinDist();
	assert(dist > 1);

	// Test unvisited graph and its size:
	int unvisited_size = h.findUnvisitedSize();
	Graph unvisited_g = h.findUnvisited();
	assert(unvisited_g.pos.size() == 4);
	assert(unvisited_g.pos.size() == 4);


}

// Basic test for Astar (not comprehensive!!!):
void testAstar() {
	
	// Test the creation of a Starnode:
	Graph g = file2graph();
	g.pos[0].visited();
	Position first = g.pos[0];
	Starnode* begin = new Starnode(first, g);
	assert(begin->current == g.pos[0]);
	assert(begin->unvisited == g.findUnvisited());

	// Test the creation of the next level of Starnodes:
	priority_queue<Starnode*, vector<Starnode*>, Greater> star_pq;
	vector<Starnode*> path;
	path.push_back(begin);
	genLevel(path[path.size() - 1], star_pq);
	assert(star_pq.size() == 4);

	// Testing multipe nextLevel creations:
	g.pos[0].visit = false;
	while (!(star_pq.top()->current == g.pos[0])) {
		path.push_back(star_pq.top());
		star_pq.pop();
		genLevel(path[path.size() - 1], star_pq);
	}

	path.push_back(star_pq.top());

	return;
}


// A* TSP finder megafunction:
void Astar() {

	// Create the graph:
	Graph g = file2graph();
	Position first = g.pos[0];
	Starnode* begin = new Starnode(first, g);


	// Create the priority queue to do the A starring:
	priority_queue<Starnode*, vector<Starnode*>, Greater> star_pq;
	Starnode* path;
	path = begin;
	genLevel(path, star_pq);

	// Do the A star alorithm until the end, and then add the final start position:
	while (!(star_pq.top()->current == g.pos[0])) {
		path = star_pq.top();
		star_pq.pop();
		genLevel(path, star_pq);
	}

	path = star_pq.top();


	// Create the list of waypoints:
	vector<Position> finalpath;
	Starnode* s_node = path;
	for(int i = 0; i < g.pos.size() + 1; i++) {
		finalpath.push_back(s_node->current);
		s_node = s_node->prev;
	}

	// reverse vector to get correct order of traversal:
	reverse(finalpath.begin(), finalpath.end());

	// write to text file waypoints.txt:
	ofstream out;
	out.open("waypoints.txt");
	for (int i = 0; i < finalpath.size(); i++) {
		out << finalpath[i].x << " " << finalpath[i].y << "\n";
	}

	return;
}

// Worst case trivial permutatio generating algorithm:
// Complexity: O(n!):
// For testing purposes only!!!
void genPerms() {
	int i = 0;
	int myints[13] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

	do {
    i++;
  } while ( std::next_permutation(myints,myints+13) );

  cout << "steps: " << i << '\n';
}

