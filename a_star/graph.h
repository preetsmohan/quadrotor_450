// Written by: Rehan Nawaz
#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>


using namespace std;

// ----- DATA STRUCTURES AND SUPPORTING FUNCTIONS ----- //

class Position {
public:
	// MEMBER VARIABLES:

	// Position and visited variables:
	double x, y, z;
	bool visit;

	// MEMBER FUNCTIONS:

	// Default Constructor:
	Position() : x(0), y(0), z(0), visit(0) {}

	// Constructor:
	Position(double x_in, double y_in, double z_in) : x(x_in), y(y_in), z(z_in), visit(0) {}

	// Copy Constructor:
	Position(const Position& p) : x(p.x), y(p.y), z(p.z), visit(p.visit) {}

	// Calculating Euclidean 3D distance:
	double dist(Position f);

	// Set true to visited:
	void visited();

	// Set false to visited:
	void unVisited();

	// OPERATORS:

	Position& operator= (const Position& rhs);

	bool operator==(const Position& rhs);


};

// Graph, containing array of positions and size:
class Graph {
public:
	// MEMBER VARIABLES:

	vector<Position> pos;

	// MEMBER FUNCTIONS:

	// Constructor:
	Graph() {}

	// Copy Constructor:
	Graph(const Graph& g) : pos(g.pos) {}

	// Find the minimum distance of unvisited nodes in graph:
	double findMinDist();

	// Return all unvisited graph nodes as a subgraph:
	Graph findUnvisited();

	// Return all unvisited graph nodes:
	int findUnvisitedSize();

	// Resets all nodes as unvisited:
	void resetGraph();

	// Heuristic for finding estimated distance of unvisited nodes:
	double heuristic();

	// OPERATORS:

	Graph& operator= (const Graph& rhs);

	bool operator==(const Graph& rhs);

};



//----- GENERAL FUNCTIONS ----- //

// Creating graph from input file:
// Formatted: x, y, z\n
Graph file2graph();


#endif