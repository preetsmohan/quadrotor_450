
// A Star library:
#ifndef A_STAR_H
#define A_STAR_H

#include "graph.h"
#include <queue>

using namespace std;

// Linked node used for creating graph for A* TSP:
class Starnode {
public:
	// MEMBER VARIABLES:	

	// Current position, list of unvisited nodes, 
	// cuurent cost from start to ndde, and heuristic cost:
	Position current;
	Graph unvisited;
	double heuristic_cost;
	double current_cost;
	Starnode* prev;

	// MEMBER FUNCTIONS:

	// Constructor:
	Starnode() {}

	// Constructor with input:
	Starnode(Position p, Graph g) : current(p), unvisited(g.findUnvisited()), heuristic_cost(g.heuristic()),
		current_cost(0), prev(NULL) {};

	// Constructor with more input:
	Starnode(Position p, Graph g, Starnode* previous) : current(p), unvisited(g.findUnvisited()), heuristic_cost(g.heuristic()),
		current_cost(0), prev(previous) {};

	// Copy Constructor:
	Starnode(const Starnode& s) {
		current = s.current;
		unvisited = s.unvisited;
		heuristic_cost = s.heuristic_cost;
		current_cost = s.current_cost;
	}

	// OPERATORS:

	// Assignment operator:
	// Starnode& operator= (const Starnode& rhs);

};

// Comparator class for min priority queue for Starnode:
class Greater {
public:	
	bool operator() (const Starnode* l, const Starnode* r) const {
		return l->heuristic_cost + l->current_cost > r->heuristic_cost + r->current_cost;
	}
};


// Generate the next level of Starnodes for traversal:
// Note: This priority queue is a min prioirty queue using heurisitics for ordering:
void genLevel(Starnode* sn, priority_queue<Starnode*, vector<Starnode*>, Greater>& pq);



#endif