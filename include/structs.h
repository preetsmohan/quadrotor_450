#ifndef STRUCTS_H
#define STRUCTS_H
#include <stdint.h>
#define RBUFFER_BUFF_SIZE     256

typedef struct
{
	float data[9]; // Indicies defined in auto_control.h
} SensorReadings_t; 

typedef struct{
	SensorReadings_t buff[RBUFFER_BUFF_SIZE]; 
	SensorReadings_t * inP;  
	SensorReadings_t * outP; 
	uint16_t count; 
} rbuffer_t;

#endif