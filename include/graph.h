// Written by: Rehan Nawaz
#ifndef GRAPH_H
#define GRAPH_H

#include <math.h>
#include <stdio.h>
#include <pthread.h>
#include "structs.h"

extern pthread_mutex_t mutex;
extern pthread_mutex_t fence_mutex;

extern int fence;

//---------- DATA TYPES FOR GRAPH, WITH ASSOCIATED FUNCTIONS ---------- //

// ----- DATA STRUCTURES AND SUPPORTING FUNCTIONS ----- //
//  visited is 1 when true, 0 when false
typedef struct {
	double x, y, z;
	int visited;
	int64_t start_time;
} Position;

// Graph, containing array of positions and size:
typedef struct {
	Position pos[10];
	unsigned int size;
} Graph;

// Setting position and initializing it as unvisited:
void setPos(Position* p, double x, double y, double z);// Position, containing {x, y, z} coordinates:
// Initialize the graph:
void initGraph(Graph* g);

void visited(Position* p);

void put_in(Graph* g, Position p, int index);
// push back into graph:
void push_back(Graph* g, Position p);

// remove from graph at position pos:
void remove_pos(Graph* g, int pos);

// resets visited nodes in graph as unvisited:
void graphReset(Graph* g);

// find minimum distance edge from unvisited nodes:
double findMinDist(Graph* g);



//----- GENERAL FUNCTIONS ----- //

// Calculating Euclidean 3D distance:
double dist(Position i, Position f);

extern Graph path;
extern Graph astar_path;

double heuristic(Graph* g);
double AStar(Position begin, Graph g);

#endif