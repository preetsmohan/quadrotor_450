#ifndef AUTO_CONTROL_H
#define AUTO_CONTROL_H

/******************************************************************************
* PWM 
******************************************************************************/
#define EXTERN extern
// PWM signal limits and neutral (baseline) settings
#include "structs.h"

// THRUST
#define thrust_PWM_up 		1575 // Upper saturation PWM limit.
//#define thrust_PWM_base 	1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 	1425 // Lower saturation PWM limit. 
#define thrust_m_PWM 		-(thrust_PWM_up - thrust_PWM_down)/2 // Was (thrust_PWM_up - thrust_PWM_down)/2
  
// ROLL
#define roll_PWM_left 		1620  // Left saturation PWM limit.
//#define roll_PWM_base 		1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 		1380 //Right saturation PWM limit. 
#define roll_m_PWM			(roll_PWM_left - roll_PWM_right)/2 // Was negative 

// PITCH
#define pitch_PWM_forward 	1620  // Forward direction saturation PWM limit.
//#define pitch_PWM_base 		1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 	1380 // Backward direction saturation PWM limit. 
#define pitch_m_PWM			-(pitch_PWM_forward - pitch_PWM_backward)/2
// YAW
#define yaw_PWM_ccw 		1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
//#define yaw_PWM_base 		1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 			1425 // Clockwise saturation PWM limit (cw = yaw right). 
#define yaw_m_PWM			(yaw_PWM_ccw - yaw_PWM_cw)/2
	
/******************************************************************************
* CONTROL LAW GAINS 
******************************************************************************/
#define KP_X		-5.1
#define KD_X		-1
#define KI_X		0 //1

#define KP_Y		5.1
#define KD_Y		1
#define KI_Y		0 //-1

#define KP_Z		200 //100
#define KD_Z		90 //30
#define KI_Z		14 // 2

#define KP_YAW		0 	
#define KD_YAW		0
#define KI_YAW		0

#define I_WIDTH		100
#define Grav		9.81


/******************************************************************************
* AUTO CONTROL DATA INPUTS
******************************************************************************/
#define T		0
#define X 		1
#define Y 		2 
#define Z 		3 
#define YAW 	4 
#define XDOT 	5
#define YDOT 	6
#define ZDOT 	7
#define YAWDOT 	8 

/******************************************************************************
* AUTO CONTROL CHANNELS
******************************************************************************/
#define THRUST	0
#define ROLL	1
#define PITCH 	2	
#define YAW 	3
 

/******************************************************************************
* functions
******************************************************************************/ 
void auto_control(float *pose, float t, float *set_points, int16_t* channels_ptr);
void moving_integral(SensorReadings_t * n); 
void control_law_2_PWM(float * u, int16_t * pwm);
void controller(SensorReadings_t * n, SensorReadings_t * ref, int16_t * channels);

#endif 