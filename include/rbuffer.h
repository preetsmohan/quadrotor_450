/*
*
*	Project: 	Tablesat: Ring Buffer 
*	Author: 	Rehan Nawaz and Joshua McCready 
* 	Email:		jmccread@umich.edu
*	Date:		3/23/2016
*
*/ 
// gill -9 PID
// ps -ef | grep ??? 
// 1st col is User ID
#ifndef RBUFFER_H
#define RBUFFER_H
#include <stdint.h>
#include "auto_control.h"
#include "structs.h"

void rbuffer_clear(rbuffer_t *in_ptr);
SensorReadings_t rbuffer_get(rbuffer_t *in_ptr);
uint16_t rbuffer_put(rbuffer_t *in_ptr, SensorReadings_t * in);
uint16_t rbuffer_count(rbuffer_t *in_ptr);
uint16_t rbuffer_free(rbuffer_t *in_ptr);
void copy_2_SensorReadings_t(float * pose, float x, SensorReadings_t * in); 
#endif
