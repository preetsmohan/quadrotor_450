#ifndef PATH_FINDING_H
#define PATH_FINDING_H

#define NUM_STEPS			10
#define desired_step_time 	1
#define desired_hold_time	5

extern int64_t hold_time;
extern int64_t start_time;

extern int read_first; //0 for false, 1 for true
extern int reached_next; //0 for false, 1 for true
extern int at_end;
extern int at_step;

double current_x;
double current_y;
double current_z;

double wn1_x;
double wn1_y;
double wn1_z;

double step_x;
double step_y;
double step_z;

double wn_x;
double wn_y;
double wn_z;

double x_diff;
double y_diff;
double z_diff;



#endif