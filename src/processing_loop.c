// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#include "../include/quadcopter_main.h"
#include "../include/path_finding.h"
#include <math.h>
#include <stdlib.h>

void read_config(char *);
int processing_loop_initialize();
int run_again = 1; 

int at_end = 0;
int at_step = 0;
int reached_next = 0;
int64_t hold_time = 0;
int read_first = 0; 
int64_t start_time = 0;
/*
 * State estimation and guidance thread 
*/
void *processing_loop(void *data){

  int hz = PROC_FREQ;
  processing_loop_initialize();

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;
  //grab waypoints

  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    
    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    //printf("mc_time_step: %lf", mc_time_step);
    //if(mc_time_step > 1.0E-7 && mc_time_step < 1) // Not Working, time is given in u seconds not seconds 
    if(mc_time_step > 0 && mc_time_step < 1000000) // Should work now, time in seconds 
    {
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
    }

  if (localstate.fence_on == 1)
  { // make an if statement (use state-> instead of local state.)
      // update desired state
    if (run_again) 
    {
      update_set_points(localstate.pose, localstate.set_points,1);  // only have this
      run_again = 0; 

    }
  }
  else
  {
    run_again = 1; 
  }


    //////////// Get rid this maybe end /////////////////

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);

  } // end while processing_loop()

  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int first_time){
  //Add your code here to generate proper reference state for the controller
  int64_t cur_time = utime_now() * pow(10,6);
  int time_held = cur_time - start_time;

  current_x = pose[0];
  current_y = pose[1];
  current_z = pose[2];

  set_points[0] = current_x;
  set_points[1] = current_y;
  set_points[2] = current_z;
  set_points[4] = 0;
  set_points[5] = 0;
  set_points[6] = 0;


/*if(!reached_next && time_held <= desired_step_time){
  set_points[0] = step_x;
  set_points[1] = step_y;
  set_points[2] = step_z;
  set_points[4] = 0;
  set_points[5] = 0;
  set_points[6] = 0;
}
else if(reached_next && time_held <= desired_hold_time){
  set_points[0] = wn_x;
  set_points[1] = wn_y;
  set_points[2] = wn_z;
  set_points[4] = 0;
  set_points[5] = 0;
  set_points[6] = 0;
}
 //---------------------------------------
else{
  time_held = 0;
  if(!at_end){
    if(reached_next){
      if(!read_first){
        fscanf(waypoint_file, "%d", "%d", "%d", wn1_x, wn1_y, wn1_z);
        fscanf(waypoint_file, "%d", "%d", "%d", wn_x, wn_y, wn_z);
        read_first = 1;
      }
      else{
        wn1_x = wn_x;
        wn1_y = wn_y;
        wn1_z - wn_z;
        fscanf(waypoint_file, "%d", "%d", "%d", wn_x, wn_y, wn_z);
      }
      x_diff = (wn_x - wn1_x)/NUM_STEPS;
      y_diff = (wn_y - wn1_y)/NUM_STEPS;
      z_diff = (wn_z - wn1_z)/NUM_STEPS;
      step_x = wn1_x;
      step_y = wn1_y;
      step_z = wn1_z;
    }
    if(at_step != NUM_STEPS){
      step_x += x_diff;
      step_y += y_diff;
      step_z += z_diff;
      at_step++;

    }else{
      at_step = 0;
      reached_next = 1;
      if(wn_x == 0 && wn_y == 0 && wn_z == 0){
        at_end = 1;
      }
    }
  start_time = utime_now() * pow(10,6);
  }
}*/

  return 0;
}




/**
 * processing_loop_initialize()
*/
int processing_loop_initialize()
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));

  // Read configuration file
  char blah[] = "config.txt";
  read_config(blah);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  pthread_mutex_lock(&state_mutex);
  fence = 0;
  pthread_mutex_unlock(&state_mutex);
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;

  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;
}

void read_config(char* config){
  // open configuration file
  FILE* conf = fopen(config,"r");
  // holder string
  char str[1000];

  // read in the quadrotor initial position
  fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);

  // ADD YOUR CODE HERE

  fclose(conf);
}
