#include "../include/rbuffer.h"
	
/*
*	Name:		void rbuffer_clear(rbuffer_t *in_ptr)
*	Requires:	in_ptr is not NULL
* 	Effects:	Resets the ring buffer
*/ 
void rbuffer_clear(rbuffer_t *in_ptr)
{
	in_ptr->inP = &(in_ptr->buff[0]);
  	in_ptr->outP = &(in_ptr->buff[0]);
  	in_ptr->count = 0;
}

/*
*	Name:		SensorReadings_t rbuffer_get(rbuffer_t *in_ptr)
*	Requires:	in_ptr is not NULL
* 	Effects:	At least 1 elt in the buffer:
*					Returns result at outP, wraps around if needed and 
*					decriments count
*				Empty buffer:
*					returns result with timestamp of -1; 
*/ 
SensorReadings_t rbuffer_get(rbuffer_t *in_ptr)
{
	SensorReadings_t result; 

	// Non-empty buffer
	if(in_ptr->count)
	{
		result = *(in_ptr->outP);
		in_ptr->outP++; 

		// Wrap around
		if ((in_ptr->outP) > &(in_ptr->buff[RBUFFER_BUFF_SIZE - 1]))
		{
			in_ptr->outP = &(in_ptr->buff[0]);
		}

		in_ptr->count--;
		return result; 
	}
	result.data[T] = -1; 
	return result; 
}

/*
*	Name:		uint16_t rbuffer_put(rbuffer_t *in_ptr, SensorReadings_t * in)
*	Requires:	Both input pointers are not NULL
* 	Effects:	With room in buffer:
*					Puts data contained in "in" into in_ptr->inP and incriments 
*					inP with wrap around, incriments count  
*				With full buffer:
*					Returns 0
*/ 
uint16_t rbuffer_put(rbuffer_t *in_ptr, SensorReadings_t * in)
{
	// buffer withe space left
	if(in_ptr->count < RBUFFER_BUFF_SIZE)
	{
		*(in_ptr->inP) = *in; 
		(in_ptr->inP)++;

		// Wrap around
		if ((in_ptr->inP) > &(in_ptr->buff[RBUFFER_BUFF_SIZE -1])) 
			in_ptr->inP = &(in_ptr->buff[0]);

		in_ptr->count++;
		return 1; 
	}
	// buffer full
	else {return 0;}
}

/*
*	Name:		uint16_t rbuffer_count(rbuffer_t *in_ptr)
*	Requires:	in_ptr is not NULL
* 	Effects:	Returns how many elements are in the ring buffer
*/ 
uint16_t rbuffer_count(rbuffer_t *in_ptr)
{
	return in_ptr->count;
}


/*
*	Name:		uint16_t rbuffer_free(rbuffer_t *in_ptr)
*	Requires:	in_ptr is not NULL
* 	Effects:	Returns how many elements are left in the ring buffer
*/ 
uint16_t rbuffer_free(rbuffer_t *in_ptr)
{
	return RBUFFER_BUFF_SIZE - in_ptr->count;
}

/*
*	Name: 		copy_2_SensorReadings_t(float * pose, float T,  SensorReadings_t * in)
*	Effects:	Copies data from auto_control to ring buffer data type 
*/
void copy_2_SensorReadings_t(float * pose, float x,  SensorReadings_t * in)
{
	int i = 1;
	in->data[T] = x; 
	for(i=1; i<9; i++)
	{
		in->data[i] = pose[i];
	}
}
