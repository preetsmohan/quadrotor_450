//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"
#include "../include/rbuffer.h"
#include "../include/auto_control.h"
//#include "../include/auto_control.h"

// Global Variable 
rbuffer_t Rbuf; 
SensorReadings_t INT_SUM; 
SensorReadings_t last_samp; 
int16_t run_once = 1;

//#define DEBUG 1

// Define outer loop controller 
// Outer loop controller to generate PWM signals for the Naza-M autopilot

// pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
// set_points (size 8):  reference state (you need to set this!) 
//                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

// channels_ptr (8-element array of PWM commands to generate in this function)
// Channels for you to set:
// [0] = thrust
// [1] = roll
// [2] = pitch
// [3] = yaw
void auto_control(float *pose, float t, float *set_points, int16_t* channels_ptr)
{  
  SensorReadings_t n;
  SensorReadings_t desired;  
  int i = 0; 

  #ifdef DEBUG
  set_points[0] = 1;
  set_points[1] = 1; 
  set_points[2] = 1; 
  set_points[3] = 0; 
  set_points[4] = 0; 
  set_points[5] = 0; 
  set_points[6] = 0; 
  set_points[7] = 0; 
  #endif 

  // Copy to sensor readings type
  copy_2_SensorReadings_t(pose, t, &n); 
  copy_2_SensorReadings_t(set_points, t, &desired);
  /*
  n.data[XDOT] = (n.data[X] - last_samp.data[X]); 
  n.data[YDOT] = (n.data[Y] - last_samp.data[Y]); 
  n.data[ZDOT] = (n.data[Z] - last_samp.data[Z]); 
  n.data[YAWDOT] = (n.data[YAW] - last_samp.data[YAW]); 
  pose[4] = n.data[XDOT]; 
  pose[5] = n.data[YDOT]; 
  pose[6] = n.data[ZDOT]; 
  pose[7] = n.data[YAWDOT];  */

 
  // Do initialization to zero just to be safe 
  if (run_once)
  {
    for(i = 1; i < 8; i++){
    INT_SUM.data[i] = 0; 
    last_samp.data[i] = 0; }
    run_once = 0; 
    rbuffer_clear(&Rbuf);
    /*n.data[XDOT] = 0; 
    n.data[YDOT] = 0; 
    n.data[ZDOT] = 0; 
    n.data[YAWDOT] = 0; 
    thrust_PWM_base = channels_ptr[0];
    roll_PWM_base = channels_ptr[1];
    pitch_PWM_base = channels_ptr[2];
    yaw_PWM_base = channels_ptr[3];*/
  }
  printf("pos: t = %lf, x = %.4f, y = %.4f , alt = %.4f, xdot = %.4f, ydot = %.4f, altdot = %.4f \n", n.data[T],n.data[X], n.data[Y], n.data[Z], n.data[XDOT], n.data[YDOT], n.data[ZDOT]);
  printf("des_pos: t = %lf, x = %.4f, y = %.4f , alt = %.4f, xdot = %.4f, ydot = %.4f, altdot = %.4f\n", desired.data[T], desired.data[X], desired.data[Y], desired.data[Z], desired.data[XDOT], desired.data[YDOT], desired.data[ZDOT]);

  controller(&n, &desired, channels_ptr);


  last_samp = n; 
  return;
}

void moving_integral(SensorReadings_t * error)
{ 
  // Up to width do not remove any 5
  SensorReadings_t got; 
  float delT = double (error->data[T] - last_samp.data[T]);

  if(rbuffer_count(&Rbuf) < (I_WIDTH-1))
  {
    INT_SUM.data[X] = INT_SUM.data[X] + error->data[X]*delT; 
    INT_SUM.data[Y] = INT_SUM.data[Y] + error->data[Y]*delT;
    INT_SUM.data[Z] = INT_SUM.data[Z] + error->data[Z]*delT; 
    INT_SUM.data[YAW] = INT_SUM.data[YAW] + error->data[YAW]*delT;  
    rbuffer_put(&Rbuf, error);
  }
  else
  {
    got = rbuffer_get(&Rbuf); 
    INT_SUM.data[X] = INT_SUM.data[X] - got.data[X]*delT; // Assumes delT is consistant 
    INT_SUM.data[Y] = INT_SUM.data[Y] - got.data[Y]*delT;
    INT_SUM.data[Z] = INT_SUM.data[Z] - got.data[Z]*delT;  
    INT_SUM.data[YAW] = INT_SUM.data[YAW] - got.data[YAW]*delT;   


    INT_SUM.data[X] = INT_SUM.data[X] + error->data[X]*delT; 
    INT_SUM.data[Y] = INT_SUM.data[Y] + error->data[Y]*delT;
    INT_SUM.data[Z] = INT_SUM.data[Z] + error->data[Z]*delT; 
    INT_SUM.data[YAW] = INT_SUM.data[YAW] + error->data[YAW]*delT; 

    rbuffer_put(&Rbuf, error);
  }
  return; 
}

void controller(SensorReadings_t * n, SensorReadings_t* ref, int16_t * channels)
{
  SensorReadings_t err; 
  float u[4]; 
  int i; 


  err.data[T] = n->data[T]; 
  for(i = 1; i < 8; i++)
  {
    err.data[i] = ref->data[i] - n->data[i];  
  }


  moving_integral(&err);
  u[0] = KP_X * err.data[X] + KD_X * err.data[XDOT] + KI_X * INT_SUM.data[X]; 
  u[1] = KP_Y * err.data[Y] + KD_Y * err.data[YDOT] + KI_X * INT_SUM.data[Y]; 
  u[2] = KP_Z * err.data[Z] + KD_Z * err.data[ZDOT] + KI_Z * INT_SUM.data[Z];
  u[3] = KP_YAW * err.data[YAW] + KD_YAW * err.data[YAWDOT] + KI_YAW * INT_SUM.data[YAW];
  printf("u_x = %f , u_y = %f, u_z = %f u_yaw = %f\n", u[0], u[1], u[2], u[3]);
  control_law_2_PWM(u, channels);
}

void control_law_2_PWM(float u[4], int16_t * pwm) // Both size 4 
{
 pwm[0] = thrust_m_PWM*u[2]+thrust_PWM_base;            // Thrust 

 if (pwm[0] > thrust_PWM_up) pwm[0] = thrust_PWM_up;
 if (pwm[0] < thrust_PWM_down) pwm[0] = thrust_PWM_down; 

 pwm[1] = roll_m_PWM*u[1]+roll_PWM_base;                // Roll

 if (pwm[1] > roll_PWM_left) pwm[1] = roll_PWM_left;
 if (pwm[1] < roll_PWM_right) pwm[1] = roll_PWM_right; 

 pwm[2] = pitch_m_PWM*u[0] + pitch_PWM_base;            // Pitch

 if (pwm[2] > pitch_PWM_forward) pwm[2] = pitch_PWM_forward; 
 if (pwm[2] < pitch_PWM_backward) pwm[2] = pitch_PWM_backward; 

 pwm[3] = yaw_m_PWM*u[3] +yaw_PWM_base; // Yaw
 if (pwm[3] > yaw_PWM_ccw) pwm[3] = yaw_PWM_ccw;  
 if (pwm[3] < yaw_PWM_cw) pwm[3] = yaw_PWM_cw;
}