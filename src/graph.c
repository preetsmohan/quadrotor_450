// Written by: Rehan Nawaz
#include <math.h>
#include <stdio.h>
#include <pthread.h>

#include "../include/graph.h"

pthread_mutex_t fence_mutex;
int fence;

// Setting position and initializing it as unvisited:
void setPos(Position* p, double x, double y, double z) {
	p->x = x;
	p->y = y;
	p->z = z;
	p->visited = 0;
		p->start_time = 0;
}

// Initialize the graph:
void initGraph(Graph* g) {
	g->size = 0;
}

void visited(Position* p){
	p->visited = 1;
}

void put_in(Graph* g, Position p, int index){
	g->pos[index] = p;

	// is this necessary (?)
	g->pos[index].x = p.x;
	g->pos[index].y = p.y;
	g->pos[index].z = p.z;
}
// push back into graph:
void push_back(Graph* g, Position p) {
	g->pos[g->size] = p;

	// is this necessary (?)
	g->pos[g->size].x = p.x;
	g->pos[g->size].y = p.y;
	g->pos[g->size].z = p.z;
	// end (?)

	g->size++;
}

// remove from graph at position pos:
void remove_pos(Graph* g, int pos) {
	if (pos >= g->size || pos < 0) {
		printf("%s\n", "Error: trying to delete outside range of graph!");
		return;
	}
	
	else {
		// delete position by incrementing array backwards from pos:
		unsigned int i = pos;
		for (i; i < g->size; ++i){
			g->pos[i] = g->pos[i + 1];
		}
		g->size--;
	}
}

// resets visited nodes in graph as unvisited:
void graphReset(Graph* g) {
	unsigned int i = 0;
	for (i; i < g->size; ++i) {
		g->pos[i].visited = 0;
	}
}

//----- GENERAL FUNCTIONS ----- //

// Calculating Euclidean 3D distance:
double dist(Position i, Position f) {
	return pow(pow(i.x - f.x, 2.0) + pow(i.y - f.y, 2.0) + pow(i.z - f.z, 2.0), 0.5);
}

// find minimum distance edge from unvisited nodes:
double findMinDist(Graph* g) {
	double minDist = -1;
	int i, j = 0;
	int countUnvisited = 0;

	for (i; i < g->size; i++) {
		if (!g->pos[i].visited) {
			countUnvisited++;
			for (j = i + 1; j < g->size; j++) {
				if (!g->pos[j].visited) {
					if (minDist == -1 || dist(g->pos[i], g->pos[j]) < minDist) {
						minDist = dist(g->pos[i], g->pos[j]);
					}
				}
			}
		}
	}

	return (countUnvisited - 1) * minDist;
}

// Heuristic function for determining cost of traversal from current 
//  position to end position 
//  (currently using minimum edge distance * number of remaining edges):
double heuristic(Graph* g) {
	// return -1 if graph is empty (we have reached goal):
	if (g->size == 0) { return -1; }

	// calculate the length of remaining edges in graph:
	else {
		return findMinDist(g);
	}
}



// Applying A* search algorithm, with MST of remaining  
//  positions as heurisitic:
double AStar(Position begin, Graph g) {
	

	// Placeholder for compilation (change!):
	return -1;
}
